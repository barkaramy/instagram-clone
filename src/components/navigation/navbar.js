import React, { useRef } from 'react';
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faSearch, faHeart } from '@fortawesome/free-solid-svg-icons'
import { faPlusSquare } from '@fortawesome/free-regular-svg-icons'
import imgTest from '../../assets/images/insta-logo.png'

function Navbar(props) {
    const inputEl = useRef(null);

    const openFile = () => {
        inputEl.current.click()
    }
    return (
        <NavbarContainer>
            <FontAwesomeIcon icon={faHome} />
            <FontAwesomeIcon icon={faSearch} />
            <input style={{display: 'none'}} type='file' ref={inputEl}></input>
            <FontAwesomeIcon icon={faPlusSquare} onClick={() => openFile()} />
            <FontAwesomeIcon icon={faHeart} />
            <ProfilImg src={imgTest} />
        </NavbarContainer>
    );
}

const NavbarContainer = styled.div`
  position: fixed;
  bottom: 0;
  width: 100%;
  background: white;
  display: flex;
  height: 50px;
  justify-content: space-around;
  align-items: center;
`

const ProfilImg = styled.img`
    width: 35px;
    height: 35px;
    border-radius: 50%;
    border: solid 1px #E5E5E5;
`

export default Navbar;