import React from 'react';
import styled from 'styled-components'

function Avatar(props) {
    return (
        <AvatarContainer mode={props.mode}>
            <AvatarImage src={props.imgUrl} mode={props.mode} />
            <AvatarLabel>{props.label}</AvatarLabel>
        </AvatarContainer>
    );

}

const AvatarContainer = styled.div`
  width: 15%;
  display: flex;
  flex-direction: ${props => props.mode === 'small' ? 'row' : 'column' };
  padding: 0.5rem;
`

const AvatarImage = styled.img`
    height: ${props => props.mode === 'small' ? '30px' : '50px' };
    width: ${props => props.mode === 'small' ? '30px' : '50px' };
    border-radius: 50%;
    border: solid 0.5px;
    align-self: center;
`
const AvatarLabel = styled.label`
  font-size: 0.5rem;
  text-align: center;
`


export default Avatar;