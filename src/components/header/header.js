import React from 'react';
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCamera } from '@fortawesome/free-solid-svg-icons'
import { faPaperPlane } from '@fortawesome/free-regular-svg-icons'
import Logo from '../UI/logo/logo'

function Header() {
    return (
        <HeaderContainer>
            <FontAwesomeIcon icon={ faCamera } />
            <Logo />
            <FontAwesomeIcon icon={ faPaperPlane } />
        </HeaderContainer>

    );
}

const HeaderContainer = styled.div`
 display: flex;
 flex-direction: row;
 justify-content: space-between;
 align-items: center;
 border-bottom: 0.5px solid lightgray;
 padding: 0.5rem;
`

export default Header;