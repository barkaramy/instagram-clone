import React from 'react';
import styled from 'styled-components';

const Input = (props) => {
    const { label, icon, inputType, onChange } = props;
    return (
        <InputContainer>
            {label && <Label>{label}</Label>}
            <DivInput>
                <Inputtag type={inputType} onChange={onChange} />
                {icon}
            </DivInput>
        </InputContainer>
    );
};

const InputContainer = styled.div``;

const Label = styled.label`
    color: red;
`;

const DivInput = styled.div``;

const Inputtag = styled.input`
    border-radius: 20px;
`;

export default Input;
