import React from 'react';
import styled from 'styled-components'
import instaLogo from '../../../assets/images/insta-logo.png'

function Logo() {
    return (
        <LogoContainer src={instaLogo} />
    );
}

const LogoContainer = styled.img`
    width: 30%;
    height: 100%;
`


export default Logo;