import React, {useState} from 'react';
import styled from 'styled-components'
import Avatar  from "../avatar/avatar";
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons'
import { faHeart, faComment, faPaperPlane, faBookmark } from '@fortawesome/free-regular-svg-icons'
import imgTest from '../../assets/images/insta-logo.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function Post(props) {
    const [liked, SetLiked] = useState(false)
    const like = () => {
        SetLiked(!liked)
    }
    return (
        <div>
            <AvatarContainer>
               <Avatar mode={props.mode} imgUrl={props.imgUrl}/>
               <FontAwesomeIcon icon={faEllipsisH} />
            </AvatarContainer>
            <PostImage src={imgTest}/>
            <ActionsContainer>
                <Actions>
                    <div onClick={like}>
                        <FontAwesomeIcon icon={faHeart}/>
                    </div>
                    <FontAwesomeIcon icon={faComment} />
                    <FontAwesomeIcon icon={faPaperPlane} />
                </Actions>
                <FontAwesomeIcon icon={faBookmark} />
            </ActionsContainer>
            <LikesCounter>{liked ? 'vous aimer' : "j'aime"}</LikesCounter>
        </div>
    );
}

const AvatarContainer = styled.div`
    display: flex;
    align-items: center;
    padding: 0.5rem;
    justify-content: space-between;
`

const Actions = styled.div`
    width: 30%;
    display: flex;
    align-items: center;
    justify-content: space-between;
`

const ActionsContainer = styled.div`
    display: flex;
    align-items: center;
    padding: 0 1rem;
    justify-content: space-between;
`

const LikesCounter = styled.div`
    padding: 0.5rem 1rem;
`

const PostImage = styled.img`
    width: 375px;
    height: 440px;
`

export default Post;