import axios from 'axios';

export const auth = (username, password) => {
    const userAuthData = {
        username: username,
        password: password
    };
    const url = 'https://easy-login-api.herokuapp.com/users/login';
    return axios.post(url, userAuthData);
};