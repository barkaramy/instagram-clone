import React from 'react';
import Header from '../components/header/header'
import Navbar from "../components/navigation/navbar";

function Layout(props) {
    return (
        <>
            <Header />
            {props.children}
            <Navbar />
        </>
    );
}

export default Layout;