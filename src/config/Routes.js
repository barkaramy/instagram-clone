import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import Home from '../screens/Home';
import Authentication from "../screens/Authentication";
import Layout from '../layout/layout'

const Routes = () => {
    return (
        <Router>
            <Layout>
                <Switch>
                    <Route exact path='/' component={Authentication} />
                    <Route exact path='/home' component={Home} />
                </Switch>
            </Layout>
        </Router>
    );
};

export default Routes;