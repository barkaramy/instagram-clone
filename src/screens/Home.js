import React from 'react';
import Avatar from "../components/avatar/avatar";
import imgTest from '../assets/images/insta-logo.png'
import styled from 'styled-components'
import Post from '../components/post/post'

function Home(props) {
    return (
        <>
            <StoriesContainer>
                <Avatar imgUrl={imgTest} label='ramy barka' />
                <Avatar imgUrl={imgTest} label='ramy barka' />
                <Avatar imgUrl={imgTest} label='ramy barka' />
                <Avatar imgUrl={imgTest} label='ramy barka' />
                <Avatar imgUrl={imgTest} label='ramy barka' />
                <Avatar imgUrl={imgTest} label='ramy barka' />
                <Avatar imgUrl={imgTest} label='ramy barka' />
            </StoriesContainer>
            <Post mode='small' imgUrl={imgTest} />
            <Post mode='small' imgUrl={imgTest} />
        </>
    );
}

const StoriesContainer = styled.div`
  display: flex;
  overflow-x: scroll;
  border-bottom: 1px solid #E5E5E5;
`

export default Home;