import React, { useState } from 'react';
import styled from 'styled-components';
import { auth } from '../services/authentication';
import Input from '../components/UI/input/Input';
import Header from "../components/header/header";

const Authentication = (props) => {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const login = (e)  => {
        e.preventDefault();
        auth(userName, password)
            .then((response) => {
                localStorage.setItem('token', response.headers['x-access-token']);
                props.history.push('/home');
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <AuthenticationContainer>
            <FormContainer onSubmit={login}>
                <Input icon={null} label='username' inputType='text' onChange={(e) => setUserName(e.target.value)} />
                <Input icon={null} label='password' inputType='text' onChange={(e) => setPassword(e.target.value)} />
                <Input icon={null} label={null} inputType='submit' />
            </FormContainer>
        </AuthenticationContainer>
    );
};

const AuthenticationContainer = styled.div`
    height: 100%;
    align-items: center;
    display: flex;
    justify-content: center;
`;

const FormContainer = styled.form`
    width: 50%;
`;

export default Authentication;
